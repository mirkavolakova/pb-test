import * as OfflinePluginRuntime from "offline-plugin/runtime";

import "./index.html";
import "./index.scss";
import "./scripts/script";
import "./scripts/nav";
import "./scripts/swiper";

OfflinePluginRuntime.install();
