import MarkerClusterer from "@google/markerclusterer";

const jobCountEl = document.getElementById("jobCount");
const jobMapEl = document.getElementById("jobMap");
const jobBranchEl = document.getElementById("jobBranch");
const jobTimeEl = document.getElementById("jobTime");
const jobOffersEl = document.getElementById("jobOffers");

let jobMap;
let jobTime;

// Initialize the map
function initMap() {
  jobMap = new google.maps.Map(jobMapEl, {
    center: { lat: 49.8175, lng: 15.473 },
    zoom: 7,
  });
}

initMap();

const clusterImgPath = "http://twomanshow.co/mirka/m";
const clusterOptions = {
  imagePath: clusterImgPath,
  zoomOnClick: false,
};
const markerCluster = new MarkerClusterer(jobMap, [], clusterOptions);

const styles = markerCluster.getStyles();
for (let i = 0; i < styles.length; i++) {
  styles[i].textColor = "#ed8b00";
  styles[i].textSize = 18;
  styles[i].width = 40;
  styles[i].height = 40;
}

// Get job data
function getJobList() {
  fetch(
    "https://jsonblob.com/api/jsonBlob/f21dc114-53df-11ea-a41b-c5815882937a"
  )
    .then(res => res.json())
    .then(data => {
      jobOffersEl.innerHTML = "";
      markerCluster.clearMarkers();
      const selectedBranchText =
        jobBranchEl.options[jobBranchEl.selectedIndex].text;
      const selectedTimeValue = jobTimeEl.value;

      const filteredJobs = data.jobs.filter(jobItem => {
        if (
          (jobItem.branch === selectedBranchText ||
            jobBranchEl.value === "all") &&
          (jobItem.time === selectedTimeValue || jobTimeEl.value === "all")
        ) {
          return jobItem;
        }
        return null;
      });

      filteredJobs.forEach(jobItem => {
        let salaryTitle = "mzda";
        if (jobItem.time === "fulltime") {
          jobTime = "Plný úvazek";
        } else if (jobItem.time === "parttime") {
          jobTime = "Zkrácený úvazek";
        } else {
          jobTime = "Brigáda";
          salaryTitle = "mzda při DPP";
        }

        const newMarker = new google.maps.Marker({
          position: jobItem.coord,
          map: jobMap,
          icon: "http://twomanshow.co/mirka/marker.png",
        });
        markerCluster.addMarker(newMarker);

        console.log(markerCluster);

        jobOffersEl.innerHTML += `<li class="jobs__item">
          <h3 class="jobs__title"><span>${jobItem.title} – ${salaryTitle} </span><span class="highlight">${jobItem.salary}</span></h3>
          <div class="jobs__atts">
            <span class="jobs__branch">${jobItem.branch}</span>
            <span class="jobs__time">${jobTime}</span>
            <span class="jobs__date">Přidáno: ${jobItem.date}</span>
          </div>
        </li>`;
      });

      const jobCount = filteredJobs.length;
      jobCountEl.innerText = jobCount;

      if (jobCount === 0) {
        jobOffersEl.innerHTML = `<div class="jobs__notfound">Vašemu požadavku neodpovídá žádná z nabídek.</div>`;
      }
    });
}

// Filter jobs by branch
function initFilterByBranch() {
  jobBranchEl.addEventListener("change", () => {
    getJobList();
  });
}

// Filter jobs by time
function initFilterByTime() {
  jobTimeEl.addEventListener("change", () => {
    getJobList();
  });
}

initFilterByBranch();
initFilterByTime();
getJobList();
