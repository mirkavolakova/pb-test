// Header navigation
const isIE11 =
  !!window.MSInputMethodContext && !!document.DOCUMENT_FRAGMENT_NODE;
const items = document.querySelectorAll(
  ".header__item:not(.header__item--logo)"
);
const sublinks = document.querySelectorAll(".header__sublink");
const activeClass = "is-active";

items.forEach(function(item) {
  item.addEventListener("click", () => {
    items.forEach(item => {
      if (item.classList.contains(activeClass)) {
        item.classList.remove(activeClass);
      }
    });
    item.classList.add(activeClass);
  });
});

sublinks.forEach(function(sublink) {
  sublink.addEventListener("click", event => {
    event.preventDefault();
    sublinks.forEach(sublink => {
      if (sublink.classList.contains(activeClass)) {
        sublink.classList.remove(activeClass);
      }
    });
    sublink.classList.add(activeClass);

    let ref = sublink.href.split("#");
    ref = "#" + ref[1];

    window.scroll({
      behavior: "smooth",
      left: 0,
      top: document.querySelector(ref).offsetTop - 81,
    });
  });
});

// Footer navigation
const titles = document.querySelectorAll(".footer__title--underline");

titles.forEach(function(title) {
  title.addEventListener("click", () => {
    titles.forEach(title => {
      if (title.classList.contains(activeClass)) {
        title.classList.remove(activeClass);
      }
    });
    title.classList.add(activeClass);
  });
});
