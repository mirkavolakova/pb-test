import Swiper from "swiper/js/swiper.js";

function reasonsSwiper() {
  let swiper = new Swiper(".swiper-container", {
    slidesPerView: 1.3,
    spaceBetween: 17,
    centeredSlides: true,
    navigation: {
      prevEl: ".swiper__nav--prev",
      nextEl: ".swiper__nav--next",
    },
    pagination: {
      el: ".swiper__pagination",
      clickable: true,
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        centeredSlides: false,
      },
      1024: {
        slidesPerView: 4,
        centeredSlides: false,
      },
    },
  });
}
reasonsSwiper();
